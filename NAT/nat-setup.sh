#!/bin/bash

echo -e "Setting up cjdns NAT gateway" &&
echo "net.ipv6.conf.all.forwarding=1" >> /etc/sysctl.conf &&
sysctl -p &&
# Setting up iptables rules
ip6tables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
ip6tables -A FORWARD -i tun0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
ip6tables-save > /etc/network/ip6tables.rules &&
cd /etc/network/if-pre-up.d/ &&
wget https://file.unrelated.info/scripts/cjdns-nat/ip6tables &&
chmod +x /etc/network/if-pre-up.d/ip6tables &&
apt-get install radvd -y &&
cd /etc &&
wget -q -O - https://file.unrelated.info/scripts/cjdns-nat/ipconfig >> /etc/network/interfaces &&
wget -q -O - https://file.unrelated.info/scripts/cjdns-nat/radvd.conf >> /etc/radvd.conf &&
systemctl enable radvd &&
systemctl start radvd &&
echo -e "NAT setup completed"

