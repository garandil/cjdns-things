#!/bin/bash

apt-get install nodejs build-essential git -y &&
cd /opt &&
git clone https://github.com/cjdelisle/cjdns.git cjdns &&
NO_TEST=1 Seccomp_NO=1 ./do &&
ln -s /opt/cjdns/cjdroute /usr/bin &&
umask 077 && ./cjdroute --genconf > /etc/cjdroute.conf &&
cp contrib/systemd/cjdns.service /etc/systemd/system/ &&
systemctl enable cjdns &&
systemctl start cjdns &&
echo -e "Setup of CJDNS completed"
